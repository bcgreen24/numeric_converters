package biz.motioncode;

import org.apache.commons.lang3.StringUtils;
/**
 *
 * @author bcgreen
 */
public class NumericConversion {
    //Converts an integer to a binary number (as a string)
    public static String int2bin(int intIntToConvert) {
        String strResult = "";
        int intPlace = 1;
        while (intIntToConvert > intPlace) {
            intPlace *= 2;
        }
        while (intPlace > 0) {
            if (intIntToConvert > intPlace || intIntToConvert == intPlace) {
                intIntToConvert = intIntToConvert % intPlace;
                strResult += "1";
            } else {
                strResult += "0";
            }
            intPlace = intPlace / 2;
        }
        strResult = StringUtils.stripStart(strResult, "0");
        return strResult;
    }
    
    //Converts a binary number (as a string) to an integer
    public static int bin2int(String strBinToConvert) {
        int intResult = 0;
        int intPlace = 1;
        int intBinLength = strBinToConvert.length();
        for (int i = intBinLength; i > 0; i--) {
            if (strBinToConvert.charAt(i - 1) == '1') {
                intResult += intPlace;
            }
            intPlace *= 2;
        }
        return intResult;
    }

    //converts a hexadecimal number (as a string) to an integer
    public static int hex2int(String strHexToConvert) {
        int intResult = 0;
        int intPlace = 1;
        int intHexLength = strHexToConvert.length();
        for (int i = intHexLength - 1; i >= 0; i--) {
            switch (strHexToConvert.charAt(i)) {
                case 'A':
                    intResult += (intPlace * 10);
                    break;
                case 'B':
                    intResult += (intPlace * 11);
                    break;
                case 'C':
                    intResult += (intPlace * 12);
                    break;
                case 'D':
                    intResult += (intPlace * 13);
                    break;
                case 'E':
                    intResult += (intPlace * 14);
                    break;
                case 'F':
                    intResult += (intPlace * 15);
                    break;
                default:
                    intResult += (intPlace * (int) Character.getNumericValue(strHexToConvert.charAt(i)));
                    break;
            }
            intPlace *= 16;
        }
        return intResult;
    }

    //Converts an integer to a hexadecimal number (as a string)
    public static String int2hex(int intIntToConvert) {
        String strResult = "";
        int intPlace = 1;
        int intValue = 0;
        while (intIntToConvert >= (intPlace * 16)) {
            intPlace *= 16;
        }
        while (intPlace > 0) {
            intValue = intIntToConvert / intPlace;
            intIntToConvert = intIntToConvert % intPlace;

            switch (intValue) {
                case 10:
                    strResult += "A";
                    break;
                case 11:
                    strResult += "B";
                    break;
                case 12:
                    strResult += "C";
                    break;
                case 13:
                    strResult += "D";
                    break;
                case 14:
                    strResult += "E";
                    break;
                case 15:
                    strResult += "F";
                    break;
                default:
                    strResult += String.valueOf(intValue);
                    break;
            }
            intPlace = intPlace / 16;
        }
        return strResult;
    }
}
